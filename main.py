import logging

from scrapper import Scrapper
from writer import Writer


def main():
    logging.basicConfig(level=logging.INFO)

    reports = Scrapper.get_reports()
    Writer.write_reports(reports)


if __name__ == '__main__':
    main()
