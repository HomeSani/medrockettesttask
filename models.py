class User:
    def __init__(self, source_user, user_todos):
        self.username = source_user.get("username")
        self.email = source_user.get("email")
        self.company_name = source_user.get("company").get("name")
        self.todos = user_todos

    @property
    def todos_count(self):
        return len(self.todos)

    @property
    def completed_todos(self):
        return list(filter(lambda todo: todo.get("completed") is True, self.todos))

    @property
    def uncompleted_todos(self):
        return list(filter(lambda todo: todo.get("completed") is False, self.todos))

    @property
    def completed_todos_count(self):
        return len(self.completed_todos)

    @property
    def uncompleted_todos_count(self):
        return len(self.uncompleted_todos)
