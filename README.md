# Тестовая задача для Medrocket

## Стек:
- poetry
- requests

## Установка:
```shell
git clone https://gitlab.com/HomeSani/medrockettesttask.git
cd MedrocketTestTask
poetry install
```

## Запуск:
```shell
poetry run python main.py
```