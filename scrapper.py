import logging
from datetime import datetime

import requests

from const import USER_API_URL, TODOS_API_URL
from models import User


class Scrapper:
    @classmethod
    def get_reports(cls):
        source_users = cls._get_users()
        source_todos = cls._get_todos()
        reports = []

        for source_user in source_users:
            if not source_user:
                logging.warning("Некорректный пользователь")
                continue

            user_todos = cls._get_todos_by_user_id(source_user, source_todos)

            if not user_todos:
                logging.warning(f"Задачи для {source_user.get('username')} не найдены")
                continue

            user = User(source_user, user_todos)
            report = cls._create_report(user)

            reports.append(report)
            logging.info(f"Создан отчёт для пользователя {user.username}")

        return reports

    @staticmethod
    def _get_users():
        response = requests.get(USER_API_URL)
        response.raise_for_status()
        logging.info("Пользователи были получены")

        return response.json()

    @staticmethod
    def _get_todos():
        response = requests.get(TODOS_API_URL)
        response.raise_for_status()
        logging.info("Задания были получены")

        return response.json()

    @staticmethod
    def _get_todos_by_user_id(user, todos):
        user_id = user.get("id")
        return list(filter(lambda todo: todo.get("userId") == user_id, todos))

    @classmethod
    def _create_report(cls, user):
        filename = f"{user.username}.txt"
        current_date = datetime.now().strftime("%d.%m.%Y %H:%M")

        body = f"# Отчёт для {user.company_name}.\n"
        body += f"{user.username} <{user.email}> {current_date}\n"
        body += f"Всего задач: {user.todos_count}\n"
        body += "\n"
        body += f"## Актуальные задачи ({user.uncompleted_todos_count}):\n"
        for todo in user.uncompleted_todos:
            title = cls._strip_todo_title(todo.get('title'))
            body += f"- {title}\n"
        body += "\n"
        body += f"## Завершённые задачи ({user.completed_todos_count}):\n"
        for todo in user.completed_todos:
            title = cls._strip_todo_title(todo.get('title'))
            body += f"- {title}\n"

        return {
            "filename": filename,
            "body": body,
        }

    @staticmethod
    def _strip_todo_title(title):
        return title if len(title) < 46 else title[:46] + "..."
