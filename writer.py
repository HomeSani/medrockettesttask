import logging
import os
from datetime import datetime

from const import OUTPUT_DIR


class Writer:
    @classmethod
    def write_reports(cls, reports):
        if not os.path.exists(OUTPUT_DIR):
            os.mkdir(OUTPUT_DIR)

        for report in reports:
            filename = OUTPUT_DIR + "/" + report.get("filename")
            body = report.get("body")

            if os.path.exists(filename):
                cls._rename_exist_file(filename)

            with open(filename, "w", encoding="utf-8") as f:
                f.write(body)

        logging.info(f"Отчёты записаны в папку {OUTPUT_DIR}")

    @staticmethod
    def _rename_exist_file(filename):
        username = filename.split('/')[-1].split('.')[0]
        old_file_create_date = datetime.fromtimestamp(
            os.path.getmtime(filename)
        ).strftime("%Y-%m-%dT%H:%M")
        new_file_name = f"{OUTPUT_DIR}/old_{username}_{old_file_create_date}.txt"

        os.rename(filename, new_file_name)
        logging.info(f"Отчёт для {username} был обновлён")
